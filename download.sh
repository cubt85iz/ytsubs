#!/usr/bin/env bash

# Default for CONFIG_FILE variable.
if [ -z "$CONFIG_FILE" ]; then
    CONFIG_FILE='/ytsubs/config/config.json'
fi

if [ ! -f "$CONFIG_FILE" ]; then
    echo "Unable to locate file: $CONFIG_FILE"
    exit 1
else
    # Iterate over shows
    DESTINATION=$(jq -r '.tv_shows.destination' $CONFIG_FILE)
    SHOW_COUNT=$(jq -r '.tv_shows.subscriptions | length' $CONFIG_FILE)
    for SHOW_INDEX in $(seq 0 $(($SHOW_COUNT - 1)))
    do
        CHANNEL_URL=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].channel" $CONFIG_FILE)
        if [ ! -z "$CHANNEL_URL" ] && [ "$CHANNEL_URL" != "null" ]; then
            # Download channels
            WRITE_SUBS=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].write_subs" $CONFIG_FILE)
            FORMAT=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].format" $CONFIG_FILE)

            # Download upload year and url for videos in channel.
            yt-dlp -I "::-1" --skip-download --quiet --print-to-file "{\"year\": \"%(upload_date>%Y)s\", \"url\": \"%(original_url)s\"}," ./temp.json $CHANNEL_URL

            # Re-format document into valid JSON.
            sed -zi 's/^{/[{/' temp.json
            sed -zi 's/\(.*\),/\1]/' temp.json

            # Iterate over the years containing uploads
            while read YEAR
            do
                # Must be at least one episode if the year exists.
                EPISODE_COUNT=1

                # Iterate over the videos for the selected year.
                while read CHANNEL_VIDEO_URL
                do
                    # Add leading zero to episode if necessary.
                    FORMATTED_EPISODE_COUNT=$EPISODE_COUNT
                    if [[ $EPISODE_COUNT -lt 10 ]]; then
                        FORMATTED_EPISODE_COUNT="0$EPISODE_COUNT"
                    fi

                    # Increment episode counter
                    COUNT=$(($EPISODE_COUNT+1))

                    if [ ! -f "DESTINATION/$SHOW_NAME/Season $YEAR/$SHOW_NAME - S${YEAR}E${FORMATTED_EPISODE_COUNT}*.mp4" ]; then
                        # Build command to download video
                        BASE_CMD="yt-dlp --output=\"$DESTINATION/$SHOW_NAME/Season $YEAR/$SHOW_NAME - S${YEAR}E${FORMATTED_EPISODE_COUNT} - %(title)s.%(ext)s\" --merge-output-format mp4 --embed-metadata"
                        if [ "$WRITE_SUBS" == "true" ]; then
                            BASE_CMD+=" --write-sub"
                        fi
                        if [ ! -z "$FORMAT" ] && [ "$FORMAT" != "null" ]; then
                            BASE_CMD+=" --format \"$FORMAT\""
                        fi

                        # Execute command to download video
                        eval "$BASE_CMD $CHANNEL_VIDEO_URL"
                    else
                        echo "INFO: Skipping file ($SHOW_NAME - S${SEASON_NUM}E${FORMATTED_EPISODE_COUNT}*.mp4). It already exists"
                    fi
                done <<< "$(jq -r ".[] | select (.year == \"${YEAR}\") | .url" temp.json)"
            done <<< "$(jq -r 'map(.year) | unique | .[]' temp.json)"

            # Remove temporary file containing channel information.
            rm ./temp.json
        else
            # Iterate over seasons
            SHOW_NAME=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].name" $CONFIG_FILE)
            SEASON_COUNT=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons | length" $CONFIG_FILE)
            for SEASON_INDEX in $(seq 0 $(($SEASON_COUNT - 1)))
            do
                SEASON_NAME=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].name" $CONFIG_FILE)
                SEASON_NUM=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].season" $CONFIG_FILE)
                EPISODE_COUNT=1

                # Download playlists
                PLAYLIST_URL=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].playlist" $CONFIG_FILE)
                WRITE_SUBS=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].write_subs" $CONFIG_FILE)
                FORMAT=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].format" $CONFIG_FILE)
                MINDURATION=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].min_duration" $CONFIG_FILE)

                if [ ! -z "$PLAYLIST_URL" ] && [ "$PLAYLIST_URL" != "null" ]; then
                    # Download date and url for videos in playlist.
                    yt-dlp -I "::-1" --skip-download --quiet --match-filter "duration > 900" --print-to-file "{\"date\": \"%(upload_date)s\", \"url\": \"%(original_url)s\"}," ./temp.json $PLAYLIST_URL

                    # Re-format document into valid JSON.
                    sed -zi 's/^{/[{/' temp.json
                    sed -zi 's/\(.*\),/\1]/' temp.json

                    # Iterate over the videos sorted by upload date
                    while read PLAYLIST_VIDEO_URL
                    do
                        # Add leading zero to episode if necessary.
                        FORMATTED_EPISODE_COUNT=$EPISODE_COUNT
                        if [[ $EPISODE_COUNT -lt 10 ]]; then
                            FORMATTED_EPISODE_COUNT="0$EPISODE_COUNT"
                        fi

                        # Increment episode counter
                        COUNT=$(($EPISODE_COUNT+1))

                        if [ ! -f "$DESTINATION/$SHOW_NAME/$SEASON_NAME/$SHOW_NAME - S${SEASON_NUM}E${FORMATTED_EPISODE_COUNT}*.mp4" ]; then
                            # Build command to download video
                            BASE_CMD="yt-dlp --output=\"$DESTINATION/$SHOW_NAME/$SEASON_NAME/$SHOW_NAME - S${SEASON_NUM}E${FORMATTED_EPISODE_COUNT} - %(title)s.%(ext)s\" --merge-output-format mp4 --embed-metadata"
                            if [ "$WRITE_SUBS" == "true" ]; then
                                BASE_CMD+=" --write-sub"
                            fi
                            if [ ! -z "$FORMAT" ] && [ "$FORMAT" != "null" ]; then
                                BASE_CMD+=" --format \"$FORMAT\""
                            fi
                            if [ ! -z "$MINDURATION" ] && [ "$MINDURATION" != "null" ]; then
                                BASE_CMD+=" --match-filter \"duration > $MINDURATION\""
                            fi

                            eval "$BASE_CMD $PLAYLIST_VIDEO_URL"
                        else
                            echo "INFO: Skipping file ($SHOW_NAME - S${SEASON_NUM}E${FORMATTED_EPISODE_COUNT}*.mp4). It already exists"
                        fi
                    done <<< "$(jq -r 'sort_by(.date) | .[].url' temp.json)"

                    # Remove temporary file containing plalist information.
                    rm ./temp.json
                else
                    # Iterate over episodes
                    EPISODE_COUNT=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes | length" $CONFIG_FILE)
                    for EPISODE_INDEX in $(seq 0 $(($EPISODE_COUNT - 1)))
                    do
                        EPISODE_NAME=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes[$EPISODE_INDEX].name" $CONFIG_FILE)
                        EPISODE_NUM=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes[$EPISODE_INDEX].episode" $CONFIG_FILE)
                        EPISODE_URL=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes[$EPISODE_INDEX].url" $CONFIG_FILE)
                        if [ ! -f "$DESTINATION/$SHOW_NAME/$SEASON_NAME/$SHOW_NAME - S${SEASON_NUM}E${EPISODE_NUM} - $EPISODE_NAME.mp4" ]; then
                            if [ ! -d "$DESTINATION/$SHOW_NAME/$SEASON_NAME" ]; then
                                mkdir -p "$DESTINATION/$SHOW_NAME/$SEASON_NAME"
                            fi

                            if [ ! -z "$EPISODE_URL" ] && [ "$EPISODE_URL" != "null" ]; then
                                WRITE_SUBS=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes[$EPISODE_INDEX].write_subs" $CONFIG_FILE)
                                FORMAT=$(jq -r ".tv_shows.subscriptions[$SHOW_INDEX].seasons[$SEASON_INDEX].episodes[$EPISODE_INDEX].format" $CONFIG_FILE)

                                BASE_CMD="yt-dlp --output=\"$DESTINATION/$SHOW_NAME/$SEASON_NAME/$SHOW_NAME - S${SEASON_NUM}E${EPISODE_NUM} - $EPISODE_NAME.%(ext)s\" --merge-output-format mp4 --embed-metadata"
                                if [ "$WRITE_SUBS" == "true" ]; then
                                    BASE_CMD+=" --write-sub"
                                fi
                                if [ ! -z "$FORMAT" ] && [ "$FORMAT" != "null" ]; then
                                    BASE_CMD+=" --format \"$FORMAT\""
                                fi

                                eval "$BASE_CMD $EPISODE_URL"
                            else
                                echo "INFO: Skipping episode ($SHOW_NAME - S${SEASON_NUM}E${EPISODE_NUM} - $EPISODE_NAME). No URL provided."
                            fi
                        else
                            echo "INFO: Skipping file ($SHOW_NAME - S${SEASON_NUM}E${EPISODE_NUM} - $EPISODE_NAME.mp4). It already exists"
                        fi
                    done
                fi
            done
        fi
    done
fi