FROM python:latest

RUN pip install yt-dlp
COPY download.sh /ytsubs/download.sh
RUN chmod +x /ytsubs/download.sh

RUN apt-get update && \
    apt-get -y upgrade
RUN apt-get install -y \
        ffmpeg \
        aria2 \
        jq && \
    rm -rf /var/lib/apt/lists/*

RUN test -d /ytsubs/config || mkdir /ytsubs/config
VOLUME /ytsubs/config

RUN mkdir /ytsubs/videos
VOLUME /ytsubs/videos

WORKDIR /ytsubs
ENTRYPOINT ["./download.sh"]