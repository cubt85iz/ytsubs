# YTSubs

Uses yt-dlp, download script and configuration file to download multiple videos from online sources.

# Configuration File

Environment Variable: `CONFIG_FILE`
Default: `/ytsubs/config/config.json`

Sample
```json
{
    "tv_shows": {
        "destination": "/ytsubs/videos",
        "owner": "owner|uid",
        "group": "group|gid",
        "subscriptions": [
             {
                "name": "America's Test Kitchen (2000)",
                "seasons": [
                    {
                        "season": "12",
                        "name": "Season 12",
                        "episodes": [
                            {
                                "episode": "01",
                                "name": "Simply Chicken",
                                "url": "episode-url",
                                "write_subs": true
                            }
                        ]
                    },
                    {
                        "season": "17",
                        "name": "Season 17",
                        "playlist": "playlist-url",
                        "min-duration": 900
                    }
                ]
             },
            {
                "name": "Gaming Historian (2009)",
                "channel": "channel-url",
                "write_subs": true
            }
        ]
    }
}
```
